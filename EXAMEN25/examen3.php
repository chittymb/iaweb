<html>
	<body>
		<h3>NEGACIÓN DE BITS</h3>
		<p>Actualice la página para mostrar una secuencia aleatoria de bits y su complementaria</p>
	</body>
</html>

<?php 
$numero= 10;
$inicio = [];
for ($i = 0; $i < $numero; $i++) {
	$inicio[$i] = rand(0,1);
}
echo "A: ";
foreach ($inicio as $bit) {
	echo "$bit ";
}
echo "</br>";

$solucion = [];
for ($i = 0; $i < $numero; $i++){
	if ($inicio[$i] == 1){
		$solucion[$i] = 0;
	} 
	else {
		$solucion[$i] = 1;
	}
}
echo "B: ";
foreach ($solucion as $bit) {
	echo "$bit ";
}
?>