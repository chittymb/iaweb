<?php
//Crea un array introduciendo las ciudades: Madrid, Barcelona, Londres,New York, Los Ángeles y Chicago, sin asignar índices al array. Acontinuación, muestra el contenido del array haciendo un recorridodiciendo el valor correspondiente a cada índice, ejemplo:La ciudad con el índice 0 tiene el nombre de Madrid.

$ciudades[]='Madrid';
$ciudades[]='Barcelona';
$ciudades[]='Londres';
$ciudades[]='New York';
$ciudades[]='Los angeles';
$ciudades[]='Chicago';
			//COUNT numero de elementos de vector
for($i=0;$i<count($ciudades);$i++){
	Echo "Laciudad con el indice $i tiene el nombre de $ciudades[$i] <br>";
}


//Repite el ejercicio anterior pero ahora si se han de crear índices, ejemplo: El índice del array que contiene como valor Madrid es MD.

$ciudades['MD']='Madrid';
$ciudades['BA']='Barcelona';
$ciudades['LO']='Londres';
$ciudades['NY']='New York';
$ciudades['LA']='Los angeles';
$ciudades['CH']='Chicago';


foreach ($ciudades as $i=>$valor) {
	
		echo "La ciudad $valor, tiene de indice $i <br>";
	}
?>