

<?php
//Realizar un programa que muestre las películas que se han visto. Crear un array que contenga los meses de enero, febrero, marzo y abril, asignando los valores 9,12,0 y 17 respectivamente. Si en alguno de los meses no se ha visto alguna película no ha de mostrar la información de ese mes.
//	   indice   valor
$meses['enero']=9;
$meses['febrero']=12;
$meses['marzo']=0;
$meses['abril']=17;

foreach ($meses as $valor) {
	if ($valor!=0){
		echo "$valor <br>";
	}
}

//para imprimir indice + valor. ene ste caso saber las peliculas vistas en los meses.
foreach ($meses as $indice=>$valor) {
	if ($valor!=0){
		echo "Las peliculas vistas en $indice son $valor <br>";
	}
}


?>