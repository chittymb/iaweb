<!-- 19. Rellena un array de 10 enteros, con los primeros números naturales. Calcula la media de los que están en posiciones pares y muestra los impares por pantalla.-->
<?php
	$array=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	$media=0;

	echo "Los elementos en posición impar son ";
	foreach ($array as $key => $value) {
		if ($key%2==0) {
			$media=$media+$array[$key];
		}
		else echo $value.", ";
	}
	echo "<br>La media de los elementos en posición par es ".$media/5;
?>