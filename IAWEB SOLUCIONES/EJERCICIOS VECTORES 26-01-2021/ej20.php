<!-- 20. Rellena los siguientes tres arrays y júntalos en uno nuevo. Muéstralos por pantalla. Utiliza la función array_merge() -->
<?php
	$animales=["Lagartija", "Araña", "Perro", "Gato", "Ratón"];
	$numeros=["12", "34", "45", "52", "12"];
	$arboles=["Sauce", "Pino", "Naranjo", "Chopo"];

	$array=array_merge($animales, $numeros, $arboles);

	foreach ($array as $key => $value) {
		echo $value.", ";
	}	
?>