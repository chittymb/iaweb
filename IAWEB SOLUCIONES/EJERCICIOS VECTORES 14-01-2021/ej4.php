<?php
	$n = 4;
	$matriz = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]];
	echo "Diagonal principal: ";
	for ($i=0; $i < $n; $i++) { 
		echo $matriz[$i][$i]." ";
	}
	echo "<br>Diagonal secundaria: ";
	for ($i=0; $i < $n; $i++) { 
		echo $matriz[$i][$n-$i-1]." ";
	}	
?>