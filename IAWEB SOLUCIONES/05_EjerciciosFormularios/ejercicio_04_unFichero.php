<html><body bgcolor="cyan">
	<!-- Crear un documento en HTML con un formulario que contenga dos cuadros de texto:
numero1, numero2. Y un botón que envíe esos datos a un documento PHP en el que se
mostrarán los dos números enviados, su producto, división y su diferencia. -->
<h1>Ejercicio 4</h1>
<form action="ejercicio_04_unFichero.php" method="post">
Numero 1: <input required="true" type="text" name="n1" size="20">
Numero 2: <input required="true" type="text" name="n2" size="20">
<input name="multiplos" type="submit" value="Enviar">
</form>
<?php
if( isset($_POST['n1']) && isset($_POST['n2'])){
	$n1 = $_POST['n1'];
	$n2 = $_POST['n2'];
	$pr = $n1 * $n2;
	$dif = $n1 - $n2;
	$div = $n1 / $n2;
	echo "<p>El primer numero es $n1</p>";
	echo "<p>El segundo numero es $n2</p>";
	echo "<p>El producto es igual a $pr</p>";
	echo "<p>La división es igual a $div</p>";
	echo "<p>La diferencia es $dif</p>";
}
?>
</body></html>