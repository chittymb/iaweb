<!-- 24. Implementa un array asociativo con los siguientes valores y ordénalo de
menor a mayor. Muestra los valores en una tabla. $numeros=array(3,2,8,123,5,1) -->
<?php
	$numeros = array(3, 2, 8, 123, 5, 1);

	echo "<table width=30% border=1px align=center><tr><td> Valores desordenados<td>";

	foreach ($numeros as $key => $value) {
		echo "$value ";
	}

	sort($numeros); // Ordena los valores del array de menor a mayor
	echo "<tr><td>Valores ordenados<td>";

	foreach ($numeros as $key => $value) {
		echo "$value ";
	}
?>