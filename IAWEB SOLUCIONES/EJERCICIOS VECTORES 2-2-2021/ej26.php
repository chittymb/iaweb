<!-- 26. Crea un array multidimensional para poder guardar los componentes de dos familias: “Los Simpson” y “Los Griffin” dentro de cada familia ha de constar el padre, la madres y los hijos, donde padre, madre e hijos serán los índices y los índices y los nombres serán los valores. Muestra los valores de las dos familias en una lista no numerada. -->
<?php
	$familias = array( "Los Simpsons" => array("padre" => "Homer", "madre" => "Marge", "hijos" => "Bart, Lisa y Maggie."),
	 "Los Griffin" => array("padre" => "Peter", "madre" => "Lois", "hijos" => "Chris, Meg y Stewie."));

	foreach ($familias as $key => $value) {
		echo "<li>Familia ".$key.": padre ".$familias[$key]["padre"].", madre ".$familias[$key]["madre"].", hijos  ".$familias[$key]["hijos"];
	}
?>