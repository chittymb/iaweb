<!-- 28. Crea una matriz para guardar a los amigos clasificados por diferentes ciudades. Los valores serán los siguientes:
Haz un recorrido del array multidimensional mostrando los valores de tal manera que nos muestre en cada ciudad que amigos tiene. -->
<?php
	$amigos = array( "Madrid" => array("nombre" => "Pedro", "edad" => "32", "teléfono" => "91-999.99.99"),
			"Barcelona" => array("nombre" => "Susana", "edad" => "34", "teléfono" => "93-000.00.00"),
			"Toledo" => array("nombre" => "Sonia", "edad" => "42", "teléfono" => "925-09.09.09"));

	foreach ($amigos as $key => $value) {
		echo "En ".$key.": nombre ".$amigos[$key]["nombre"].", edad ".$amigos[$key]["edad"].", teléfono ".$amigos[$key]["teléfono"].".<br>";
	}
?>