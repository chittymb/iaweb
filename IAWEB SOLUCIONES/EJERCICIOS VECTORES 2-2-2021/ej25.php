<!-- 25. Crea un array con los siguientes vlaores: 5->1, 12->2, 13->56, x->42. Muestra el contenido. Cuenta el número de elementos que tiene y muéstralo por pantalla. A continuación borrar el contenido de posición 5. Vuelve a mostrar el contenido y por último elimina el array. -->
<?php
	$numeros = array(5 => 1, 12 => 2, 13 => 56, "x" => 42);

	foreach ($numeros as $key => $value) {
		echo "$numeros[$key] ";
	}

	echo "<br>El array tiene ".count($numeros)." elementos.<br> <br> Tras borrar el elemento en posición 5 el array queda así: ";

	unset($numeros[5]);

	foreach ($numeros as $key => $value) {
		echo "$numeros[$key] ";
		unset($numeros[$key]); // Va borrando el elemento hasta dejar el array vacío
	}
?>