<!-- 27. Crea un array llamado deportes e introduce los siguientes valores: futbol, baloncesto, natación, tenis. Haz el recorrido de la matriz con un for para mostrar sus valores. A continuación realiza las siguientes operaciones
· Muestra el total de valores que contiene.
· Sitúa el puntero en el primer elemento del array y muestra el valor actual, es decir, donde está situado el puntero actualmente.
· Avanza una posición y muestra el valor actual.
· Coloca el puntero en la última posición y muestra su valor.
· Retrocede una posición y muestra este valor. -->
<?php
	$deportes = array("futbol", "baloncesto", "natación", "tenis");

	echo "El array contiene ".count($deportes)." valores: ";
	foreach ($deportes as $key => $value) {
		echo "$value ";
	}

	reset($deportes);
	echo "<br>Tras situar el puntero en el primer elemento el valor actual es: ".current($deportes);

	next($deportes);
	echo "<br>Tras avanzar el puntero una posición el valor actual es: ".current($deportes);
	
	end($deportes);
	echo "<br>Tras colocar el puntero en la última posición el valor actual es: ".current($deportes);

	prev($deportes);
	echo "<br>Tras retroceder el puntero una posición el valor actual es: ".current($deportes);
?>