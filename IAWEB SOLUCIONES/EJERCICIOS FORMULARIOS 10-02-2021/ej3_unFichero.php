<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ej3</title>
</head>
<body>
	<?php
		$tope = $_POST["num"];
		if (isset($_POST["pares"])) {
			echo "Los números pares hasta $tope son ";
			for ($i=1; $i < $tope; $i++) { 
				if ($i % 2 == 0) {
					echo "$i, ";
				}
				
			}
		}else {
			echo "Los números impares hasta $tope son ";
			for ($i=1; $i < $tope; $i++) { 
				if ($i % 2 != 0) {
					echo "$i, ";
				}
				
			}
		}
	?>
	<form action="ej3_unFichero.php" method="POST">
		<p>Bienvenido, introduce un número <br> <br>
			Número: <input type="int" name="num" required/><br> <br>
		</p>
		<input type="submit" name="pares" value="Pares">
		<input type="submit" name="impares" value="Impares">
	</form>
</body>
</html>