<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ej2</title>
</head>
<body>
	<?php
		$num = $_POST["num"];
		$pot = $_POST["pot"];
		$sol = 1;
		echo "El resultado de $num elevado a $pot es: ";
		for ($i=0; $i < $pot; $i++) { 
			$sol = $sol * $num;
		}
		echo "$sol";
	?>
	<form action="ej2_unFichero.php" method="POST">
		<p>Bienvenido, introduce un número y la potencia <br> <br>
			Número: <input type="int" name="num" required/><br> <br>
			Potencia: <input type="int" name="pot" required/><br> <br>
		</p>
		<input type="submit" name="calcular" value="Calcular">
	</form>
</body>
</html>