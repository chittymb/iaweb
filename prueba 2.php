<?php
// Ejemplo de impresión de una fecha usando la instrucción echo
// echo imprime en el documento el dato que lleve como argumento
echo date("d-m-Y"); // imprime la fecha de hoy
/* La función date retorna la fecha del sistema
 d = día del mes, D = día de la semana (inglés)
 m = mes en número, M= mes en 3 caracteres (inglés)
 Y = año, y= año en 2 dígitos, w= día de la semana en número
*/
?>